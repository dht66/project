# 电商平台数据可视化实时监控系统

## 介绍
JavaScript/echarts/Vue.js/WebSocket

![最终效果图](https://images.gitee.com/uploads/images/2020/1113/233419_e7d1c280_8163329.jpeg "1.jpg")


## 安装教程


1. 启动后端工程 

- cd koa2-server;
- npm install;
- node app.js;

2. 启动前端工程

 - cd vision-dht;
 - npm install;
 - npm run serve;

## 介绍说明
- 通过 axios 请求后对数据进行排序，分页，启动定时器修改页数，更新，实现柱状图动态刷新展示功能。 
- 通过对 window resize 事件监听，重新配置图表大小有关的 echarts 配置项从而达到图表自适应功能。 
- 数据由 AJAX 请求改造成 WebSocket 连接，客户端发送约定字段到服务端，接收到服务端返回的数据 后进行图表更新或者调整 Vuex 的主题相关数据实现所有图表的主题切换

